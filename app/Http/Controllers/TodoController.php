<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    public function index()
    {
        return Auth::user()->todos;
    }

    public function store(Request $request)
    {
        $todo = new Todo();
        $todo->fill($request->all());
        $todo->user()->associate(Auth::user());
        $todo->save();

        return $todo;
    }

    public function show(Todo $todo)
    {
        return $todo;
    }

    public function update(Request $request, Todo $todo)
    {
        $todo->fill($request->all());
        $todo->user()->associate(Auth::user());
        $todo->save();

        return $todo;
    }

    public function destroy(Todo $todo)
    {
        $todo->delete();
        return ['deleted' => $todo->id];
    }
}
